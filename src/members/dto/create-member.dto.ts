import { IsNotEmpty, Length } from 'class-validator';
export class CreateMemberDto {
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  @Length(10, 32)
  tel: string;
}
